import React from "react";
import Card from "react-bootstrap/Card";
import { ImPointRight } from "react-icons/im";

function AboutCard() {
  return (
    <Card className="quote-card-view">
      <Card.Body>
        <blockquote className="blockquote mb-0">
          <p style={{ textAlign: "justify" }}>
            Hi Everyone, I am <span className="purple">Arbaaz Ghameria </span>
            from <span className="purple"> Mumbai, Maharashtra India.</span>
            <br />I am in Third year pursuing B. Tech in Information Technology from Sardar Patel Institute of Technology, Andheri West.
            <br />
            <br />
            Apart from coding, some other activities that I love to do!
          </p>
          <ul>
            <li className="about-activity">
              <ImPointRight /> Playing Games
            </li>
            <li className="about-activity">
              <ImPointRight /> Binge Watching
            </li>
            <li className="about-activity">
              <ImPointRight /> Travelling
            </li>
          </ul>

          <p style={{ color: "rgb(155 126 172)" }}>
            "Some succeed because they are destined to, but most succeed because they are determined to!" and I believe i am determined enough to succeed{" "}
          </p>
          <footer className="blockquote-footer">quote by -Henry Van Dyke</footer>
        </blockquote>
      </Card.Body>
    </Card>
  );
}

export default AboutCard;
