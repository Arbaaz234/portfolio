import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import ProjectCard from "./ProjectCards";
import Particle from "../Particle";
import leaf from "../../Assets/Projects/weatherit.png";
import emotion from "../../Assets/Projects/emotion.png";
import editor from "../../Assets/Projects/bank.jpg";
import chatify from "../../Assets/Projects/chatify.png";
import suicide from "../../Assets/Projects/suicide.png";
import bitsOfCode from "../../Assets/Projects/RMS.jpg";

function Projects() {
  return (
    <Container fluid className="project-section">
      <Particle />
      <Container>
        <h1 className="project-heading">
          My <strong className="purple">Projects </strong>
        </h1>
        <p style={{ color: "white" }}>
          Here are a few projects I've worked on recently.
        </p>
        <Row style={{ justifyContent: "center", paddingBottom: "10px" }}>
          <Col md={4} className="project-card">
            <ProjectCard
              imgPath={chatify}
              isBlog={false}
              title="Groopr"
              description="Groups are everywhere, from your friend circle to a small business team they require a platform where they can meet and share projects and thoughts while gaining information on how other people are fairing in the same topic using group categorization. Android-based application aims for an easy access and use for providing almost free of cost facility to users. Therefore, the development of team based android-based application is essentially required with more functionality than similar applications such as Slack and Discord. The primary goal of this study is the development of android-based Grouping application which can assist people to access and hone their productivity by matching them with similar people with similar interests. The application was developed in android operating system environment. Android Studio by Android is used to develop the system and the user interface. The results reveal that the system provides an easy and user-friendly interface for end-users."
            // ghLink="https://github.com/soumyajit4419/Chatify"
            // demoLink="https://chatify-49.web.app/"
            />
          </Col>

          <Col md={4} className="project-card">
            <ProjectCard
              imgPath={bitsOfCode}
              isBlog={false}
              title="Rent Management System"
              description="This project can be used by Landlords as well as Rent Collectors to lookup to tenants paying the rent and the rent collectors collecting it and details about the payment done by the tenants to the rent collector with the complete data of Landlords, Tenants, Buildings and Rent Collectors. So that makes it more efficient to use rather than keeping the hard records. This data will be centralized as the Data can be accessed from anywhere and provides the security through login page which will ask for id and password. There will also be a page to change the passwords. There is a data for Arrears which shows the name and details of the tenants who haven’t paid the rent amount yet and also shows how much amount is left for him to pay with the detail of the amount paid till. There will be 2 types of users Landlord and Rent Collectors who will have different privileges."
            // ghLink="https://github.com/soumyajit4419/Bits-0f-C0de"
            // demoLink="https://blogs.soumya-jit.tech/"
            />
          </Col>

          <Col md={4} className="project-card">
            <ProjectCard
              imgPath={editor}
              isBlog={false}
              title="Bank App Front-end Java"
              description="Banks have traditionally been in the forefront of harnessing technology to improve their products, services and efficiency. They have, over a long time, been using electronic and telecommunication networks for delivering a wide range of value added products and services. The delivery channels include direct dial – up connections, private networks, public networks etc and the devices include telephone, Personal Computers including the Automated Teller Machines, etc. With the popularity of PCs, easy access to Internet and World Wide Web (WWW), Internet is increasingly used by banks as a channel for receiving instructions and delivering their products and services to their customers. This form of banking is generally referred to as Internet Banking, although the range of products and services offered by different banks vary widely both in their content and sophistication."
            // ghLink="https://github.com/soumyajit4419/Editor.io"
            // demoLink="https://editor.soumya-jit.tech/"
            />
          </Col>

          <Col md={4} className="project-card">
            <ProjectCard
              imgPath={leaf}
              isBlog={false}
              title="WeatherIT"
              description="‘WeatherIT’ is a realtime weather forecast and details website which shows the current weather, forecast, precipitation and air quality. Along with these details, it contains two unique pages which focus on the ideal food and beverages to be consumed according the current weather and location."
            // ghLink="https://github.com/soumyajit4419/Plant_AI"
            // demoLink="https://plant49-ai.herokuapp.com/"
            />
          </Col>
          {/* 
          <Col md={4} className="project-card">
            <ProjectCard
              imgPath={suicide}
              isBlog={false}
              title="Ai For Social Good"
              description="Using 'Natural Launguage Processing' for the detection of suicide-related posts and user's suicide ideation in cyberspace  and thus helping in sucide prevention."
            // ghLink="https://github.com/soumyajit4419/AI_For_Social_Good"
            // demoLink="https://www.youtube.com/watch?v=dQw4w9WgXcQ&ab_channel=RickAstley" <--------Please include a demo link here
            />
          </Col>

          <Col md={4} className="project-card">
            <ProjectCard
              imgPath={emotion}
              isBlog={false}
              title="Face Recognition and Emotion Detection"
              description="Trained a CNN classifier using 'FER-2013 dataset' with Keras and tensorflow backened. The classifier sucessfully predicted the various types of emotions of human. And the highest accuracy obtained with the model was 60.1%.
              Then used Open-CV to detect the face in an image and then pass the face to the classifer to predict the emotion of a person."
            // ghLink="https://github.com/soumyajit4419/Face_And_Emotion_Detection"
            // demoLink="https://blogs.soumya-jit.tech/"      <--------Please include a demo link here 
            />
          </Col> */}
        </Row>
      </Container>
    </Container>
  );
}

export default Projects;
